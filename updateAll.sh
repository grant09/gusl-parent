#!/bin/sh

echo "Parent"
git pull

for i in $(cat modules) 
do
	if [ -d $i ]; then
		echo "Updating " $i
		cd $i
		git pull
		cd ..
	fi
done
