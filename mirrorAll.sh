#!/bin/sh

#ssh-add -K ~/.ssh/supernova_bitbucket_rotunda.pem
for i in gu*
do
	if [ -d $i ]; then
		echo "Mirror " $i
		cd $i
                git push --mirror ssh://grant08@bitbucket.org/supernovasports/$i
		cd ..
	fi
done
