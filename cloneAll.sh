#!/bin/sh

if [ -z "$1" ]
  then
    echo "No BitBucket username supplied"
	exit
fi

for i in `cat modules`
do
	echo "clone " $i
	git clone https://$1@bitbucket.org/grownupsoftware/$i.git
done
