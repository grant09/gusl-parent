#!/bin/sh

for i in `cat modules`
do
	if [ -d $i ]; then
		echo "Publishing " $i
		cd $i
		git pull
    ./gradlew clean compileJava publish
		cd ..
	fi
done
